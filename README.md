# Space Traffic Web3 UI

This project contains the user interface for the consensus smart contracts project to manage international data message exchanges alerts about collisions in the spatial domain.

## Folder Structure

- **src/** : Contains the application's source code.
  - **components/** : Reusable components of the application.
  - **assets/** : Static resources such as images, icons, etc.
  - **App.vue** : Main file of the application.
  - **main.js** : Entry point of the application.
- **public/** : Contains static files accessible to the public.
- **package.json** : npm configuration file.
- **README.md** : This file.

## Fonctionnalités

- Home page including a NavBar component, application data, and a Footer component.
- NavBar component with a search bar and 'Connect' button.
- Component displaying application data with a 3D globe and the latest network activities.
- Footer component with team information and copyright.

## Getting started

npm is your friend.

```bash
npm install
npm run dev
```

## Other info for developers

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Licensing

This work is licensed under the GNU LESSER GENERAL PUBLIC LICENSE
version 3 and above. All contributors accepts terms of this license.
